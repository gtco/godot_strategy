extends StaticBody2D


signal create_worker

var selected

# Called when the node enters the scene tree for the first time.
func _ready():
	selected = false


func _unhandled_input(event):
		if event is InputEventMouseButton:
			if event.button_index == BUTTON_LEFT:
				if selected and event.pressed:
					emit_signal("create_worker")


func _on_StaticBody2D_mouse_entered():
	selected = true


func _on_StaticBody2D_mouse_exited():
	selected = false

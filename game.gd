extends Node2D

var ws = preload("res://worker.tscn")

var cradle
var worker_max 
var worker_count

# Called when the node enters the scene tree for the first time.
func _ready():
	cradle = $Cradle
	worker_max = 1
	worker_count = 0


func _on_Cradle_create_worker():
	if worker_count < worker_max:
		var worker = ws.instance()
		worker_count += 1
		worker.position.x = cradle.position.x + 10
		worker.position.y = cradle.position.y
		add_child(worker)

#afternoon haze color palette
#0d1221 black
#243a56 dark blue
#2f6f87 blue
#3ca0af light blue
#cbbfb2 white
#cf9d36 yellow
#b0523f orange
#a00f0e red
